﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Xml.Linq;

namespace Syroot.NintenTools.Byaml
{
    /// <summary>
    /// Represents BYAML loading, saving, and conversion functionality.
    /// </summary>
    public static class ByamlData
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        internal const ushort MaxSupportedVersion = 3;

        // ---- FIELDS -------------------------------------------------------------------------------------------------

        internal static readonly Encoding ShiftJisEncoding;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        static ByamlData()
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            ShiftJisEncoding = Encoding.UTF8;// Encoding.GetEncoding("shift_jis");
        }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Converts the given <paramref name="obj"/> to a BYAML compatible root node.
        /// </summary>
        /// <param name="obj">The <see cref="Object"/> to convert.</param>
        /// <returns>The BYAML compatible root node.</returns>
        [DebuggerStepThrough]
        public static dynamic FromObject(object obj) => throw new NotImplementedException();

        /// <summary>
        /// Converts the given <paramref name="xDocument"/> to a BYAML compatible root node.
        /// </summary>
        /// <param name="xDocument">The <see cref="XDocument"/> to convert.</param>
        /// <returns>The BYAML compatible root node.</returns>
        [DebuggerStepThrough]
        public static dynamic FromXDocument(XDocument xDocument) => XmlConverter.FromXDocument(xDocument);

        /// <summary>
        /// Retrieves the <see cref="ByamlFormat"/> from the file with the given <paramref name="fileName"/>.
        /// </summary>
        /// <param name="fileName">The name of the file to retrieve the format information from.</param>
        /// <returns>The format information describing the BYAML encoding.</returns>
        [DebuggerStepThrough]
        public static ByamlFormat GetFormat(string fileName)
        {
            using FileStream stream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read);
            return GetFormat(stream);
        }

        /// <summary>
        /// Retrieves the <see cref="ByamlFormat"/> from the given <paramref name="stream"/>. The position of the stream
        /// will not be changed.
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> to retrieve the format information from.</param>
        /// <returns>The format information describing the BYAML encoding.</returns>
        [DebuggerStepThrough]
        public static ByamlFormat GetFormat(Stream stream) => DynamicLoader.GetFormat(stream);

        /// <summary>
        /// Retrieves the <see cref="ByamlFormat"/> from the given <paramref name="xDocument"/>.
        /// </summary>
        /// <param name="xDocument">The <see cref="XDocument"/> to retrieve the format information from.</param>
        /// <returns>The format information describing the BYAML encoding.</returns>
        [DebuggerStepThrough]
        public static ByamlFormat GetFormat(XDocument xDocument) => XmlConverter.GetFormat(xDocument);

        /// <summary>
        /// Loads the BYAML data available in the file with the given <paramref name="fileName"/>.
        /// </summary>
        /// <param name="fileName">The name of the file to load BYAML data from.</param>
        /// <returns>A dynamic object representing the root node of the BYAML data.</returns>
        [DebuggerStepThrough]
        public static dynamic Load(string fileName)
        {
            using FileStream stream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read);
            return Load(stream);
        }

        /// <summary>
        /// Loads the BYAML data available in the given <paramref name="stream"/>.
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> to load BYAML data from.</param>
        /// <returns>A dynamic object representing the root node of the BYAML data.</returns>
        [DebuggerStepThrough]
        public static dynamic Load(Stream stream) => new DynamicLoader().Load(stream);

        /// <summary>
        /// Saves the <paramref name="root"/> as BYAML data in the file with the given <paramref name="fileName"/>.
        /// </summary>
        /// <param name="fileName">The name of the file to store the BYAML data in.</param>
        /// <param name="root">The root node to save.</param>
        /// <param name="format">The <see cref="ByamlFormat"/> to encode the data in.</param>
        [DebuggerStepThrough]
        public static void Save(string fileName, dynamic root, ByamlFormat format)
        {
            using FileStream stream = new FileStream(fileName, FileMode.Create, FileAccess.Write, FileShare.None);
            Save(stream, root, format);
        }

        /// <summary>
        /// Saves the <paramref name="root"/> as BYAML data in the given <paramref name="stream"/>.
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> to store the BYAML data in.</param>
        /// <param name="root">The root node to save.</param>
        /// <param name="format">The <see cref="ByamlFormat"/> to encode the data in.</param>
        [DebuggerStepThrough]
        public static void Save(Stream stream, dynamic root, ByamlFormat format)
            => new DynamicSaver().Save(stream, root, format);

        /// <summary>
        /// Creates an <see cref="Object"/> from the BYAML data.
        /// </summary>
        /// <param name="root">The dynamic root node to convert.</param>
        /// <returns>The <see cref="Object"/> representing the BYAML data.</returns>
        [DebuggerStepThrough]
        public static object ToObject(dynamic root) => throw new NotImplementedException();

        /// <summary>
        /// Creates an <see cref="XDocument"/> from the BYAML data.
        /// </summary>
        /// <remarks>
        /// The format is trying to be compatible to Chadderz121s yamlconv utility, but uses incompatible extensions as
        /// soon as newer BYAML features are needed (additional value types, null values, references, versions 2+).
        /// </remarks>
        /// <param name="root">The dynamic root node to convert.</param>
        /// <param name="format">The <see cref="ByamlFormat"/> to encode the data in.</param>
        /// <returns>The <see cref="XDocument"/> representing the BYAML data.</returns>
        [DebuggerStepThrough]
        public static XDocument ToXDocument(dynamic root, ByamlFormat format) => XmlConverter.ToXDocument(root, format);
    }
}
