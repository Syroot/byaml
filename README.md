# NintenTools.Byaml

This is a .NET library to handle the BYAML Nintendo data file format, allowing you to read and store the files as either dynamic objects or strongly-typed with serialized classes similar to what the .NET `XmlReader` and `XmlWriter` provide.

The library is available as a [NuGet package](https://www.nuget.org/packages/Syroot.NintenTools.Byaml).

## Status

The current version of the library only supports basic version 1 BYAML files as seen in Mario Kart 8 for the Wii U.

A completely overhauled version 3.0.0 of the library is under development and will be released soon, with the following features:
- Support for version 2 and 3 BYAML files.
- Automatic detection of Switch (little endian) files.
- Automatic detection of Mario Kart 8 path support (when loading files).
- Support for `UInt32`, `Int64`, `UInt64` and `Double` nodes.
- Support for reference nodes, e.g. returning the same instance of the node if it was used multiple times. Solves circular reference issues.
- Support for Japanese text (Shift JIS encoding).
- Possibility to acquire BYAML version and file information without loading the contents.
- Preservation of dictionary element order as in the original files to create binary-equivalent exports.
- XML import and export, compatible to yamlconv as much as possible.
- Simplified object serialization features handling any class without further changes required.
- Completely rewritten parsing logic; shorter, cleaner, and faster.
- Update to BinaryData 5, parsing binary data faster and providing more features.

## Deprecation Notice

**This project aswell as other NintenTools projects is no longer updated or maintained.**
The following known issues result from this:
- Version 3 is only available as a pre-release version. Its API is completely different from version 2.
- Automatic serialization was **not added** in version 3, use the previous version 2 for any libraries or tools dependent on this.
- The wiki only documents version 2 usage. Check the Scratchpad project for version 3 sample code.
